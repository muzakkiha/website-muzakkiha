from django.shortcuts import render


def index(request):
    response = {}
    return render(request, 'home.html', response)
 
 